﻿namespace QueueContracts
{
    public interface ISampleMessage
    {
        string Message { get; set; }
    }
}
