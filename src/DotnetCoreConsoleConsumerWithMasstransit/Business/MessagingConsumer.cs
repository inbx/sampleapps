﻿using MassTransit;
using QueueContracts;
using System;
using System.Threading.Tasks;

namespace DotnetCoreConsoleConsumerWithMasstransit
{
    public class MessagingConsumer : IConsumer<ISampleMessage>
    {

        //You can inject here if you need
        //private readonly IMessageSender _MessageSender;

        public MessagingConsumer()//IMessageSender messageSender)
        {
            //_MessageSender = messageSender;
        }

        /// <summary>
        /// Get Message from queue
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Consume(ConsumeContext<ISampleMessage> context)
        {
            Console.WriteLine(context.Message.Message);
            //await _MessageSender.Send(context.Message);
        }
    }
}
