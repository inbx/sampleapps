﻿using CoreConsoleHosting.Hosting;
using GreenPipes;
using MassTransit;
using MassTransit.ExtensionsDependencyInjectionIntegration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace DotnetCoreConsoleConsumerWithMasstransit
{
    public class Startup : IConsoleStartup
    {
        /// <summary>
        /// 
        /// </summary>
        public IConfiguration Configuration { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IHostingEnvironment Environment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddScoped<MessagingConsumer>();

            services.AddMassTransit(x =>
            {
                x.AddConsumer<MessagingConsumer>();
            });

            var messageSetting = Configuration.GetSection("Messaging").Get<MessagingSetting>();

            services.AddSingleton(provider => Bus.Factory.CreateUsingRabbitMq(cfg =>
            {
                var host = cfg.Host(new Uri("rabbitmq://wolverine.rmq.cloudamqp.com/vbknszsj"), h =>
                {
                    h.Username("vbknszsj");
                    h.Password("QjAPX0m6ehoMIoUPdMmqzRmVGMNZIPcD");
                });

                cfg.ReceiveEndpoint(host, "SampleQueue1", e =>
                {
                    e.PrefetchCount = 16;
                    e.UseMessageRetry(x => x.Interval(2, 100));

                    e.LoadFrom(provider);

                });
            }));
            services.AddSingleton<IPublishEndpoint>(provider => provider.GetRequiredService<IBusControl>());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        public void Configure(IServiceProvider serviceProvider)
        {
            var bus = serviceProvider.GetService<IBusControl>();
            bus.Start();
        }

    }

    public class MessagingSetting
    {
        public string Name { get; set; }
    }

}
