﻿using CoreConsoleHosting;

namespace DotnetCoreConsoleConsumerWithMasstransit
{
    class Program
    {
        static void Main(string[] args)
        {
            BuildConsoleApp(args).Run();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IConsoleApp BuildConsoleApp(string[] args) =>
             ConsoleApp.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
