﻿using MassTransit;
using QueueContracts;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebAppFullFrameworkWithAutofac.Business;
using WebAppFullFrameworkWithAutofac.Models;

namespace WebAppFullFrameworkWithAutofac.Controllers
{
    public class HomeController : Controller
    {
        private readonly IBusiness1 _business1;
        private readonly TextWriter _writer;
        private readonly IBusControl _bus;

        /// <summary>
        /// Constructor (Inject components here)
        /// </summary>
        /// <param name="business1"></param>
        public HomeController(IBusiness1 business1, TextWriter writer, IBusControl bus)
        {
            //Set injected parameters to readonly local variables
            _business1 = business1;
            _writer = writer;
            _bus = bus;
        }

        [HttpGet]
        public ActionResult Index()
        {
            //Using injected components
            _business1.Foo();
            _writer.Write("test");
            
            return View();
        }

        [HttpPost]
        public ActionResult Index(string Message)
        {
            _bus.Publish<ISampleMessage>(new MessageModel { Message = Message });
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}