﻿using Autofac;
using Autofac.Integration.Mvc;
using MassTransit;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebAppFullFrameworkWithAutofac.Business;

namespace WebAppFullFrameworkWithAutofac
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //Start DI Container and register services
            DIRegistration();

            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        private void DIRegistration()
        {
            var builder = new ContainerBuilder();

            // Register your MVC controllers. (MvcApplication is the name of
            // the class in Global.asax.)
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // OPTIONAL: Register model binders that require DI.
            builder.RegisterModelBinders(typeof(MvcApplication).Assembly);
            builder.RegisterModelBinderProvider();

            // OPTIONAL: Register web abstractions like HttpContextBase.
            builder.RegisterModule<AutofacWebTypesModule>();

            // OPTIONAL: Enable property injection in view pages.
            builder.RegisterSource(new ViewRegistrationSource());

            // OPTIONAL: Enable property injection into action filters.
            //builder.RegisterFilterProvider();

            // OPTIONAL: Enable action method parameter injection (RARE).
            //builder.InjectActionInvoker();

            RegisterComponents(builder);

            // Set the dependency resolver to be Autofac.
            var container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));

        }

        private static void RegisterComponents(ContainerBuilder builder)
        {
            RegisterMasstransit(builder);

            // Register types that expose interfaces...
            builder.RegisterType<Business1>().As<IBusiness1>();

            // Register instances of objects you create...
            var output = new StringWriter();
            builder.RegisterInstance(output).As<TextWriter>();

            // Register expressions that execute to create objects...
            //builder.Register(c => new ConfigReader("mysection")).As<IConfigReader>();

        }

        //Cload rabbit for test

        //amqp://vbknszsj:QjAPX0m6ehoMIoUPdMmqzRmVGMNZIPcD@wolverine.rmq.cloudamqp.com/vbknszsj
        //Hostname wolverine.rmq.cloudamqp.com
        //Port    1883(8883 for TLS)
        //Username vbknszsj:vbknszsj
        // Password    QjAPX0m6ehoMIoUPdMmqzRmVGMNZIPcD

        private static void RegisterMasstransit(ContainerBuilder builder)
        {
            builder.Register(context =>
            {
                var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var host = cfg.Host(new Uri("rabbitmq://wolverine.rmq.cloudamqp.com/vbknszsj"), h =>
                    {
                        h.Username("vbknszsj");
                        h.Password("QjAPX0m6ehoMIoUPdMmqzRmVGMNZIPcD");
                    });
                });

                return busControl;
            })
                .SingleInstance()
                .As<IBusControl>()
                .As<IBus>();
        }
    }
}
