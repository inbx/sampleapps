﻿using QueueContracts;

namespace WebAppFullFrameworkWithAutofac.Models
{
    public class MessageModel : ISampleMessage
    {
        public string Message { get; set; }
    }
}