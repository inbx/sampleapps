﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAppFullFrameworkWithAutofac.Business
{
    public interface IBusiness1
    {
        void Foo();
    }
    public class Business1 : IBusiness1
    {
        public void Foo()
        {

        }
    }
}