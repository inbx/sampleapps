
-------------------------- Sample Apps adn Guids for Inbx ---------------

------------ WebAppFullFrameworkWithAutofac Start--------------

Using autofact in a full framework dotnet application

 1- Install-Package Autofac.Mvc5 
 2- in the global.asax
      var builder = new ContainerBuilder();
 3- Register Controller
   builder.RegisterControllers(typeof(MvcApplication).Assembly);
 4- Register Service and components
    builder.RegisterType<Business1>().As<IBusiness1>();
 5- Build container
     var container = builder.Build();
     DependencyResolver.SetResolver(new AutofacDependencyResolver(container));



Using Masstransit in full framework dotnet aplication 

1- Install Packages : MassTransit, MassTransit.AutoFac, and MassTransit.RabbitMQ
2- Register Masstransit
builder.Register(context =>
            {
                var busControl = Bus.Factory.CreateUsingRabbitMq(cfg =>
                {
                    var host = cfg.Host(new Uri("rabbitmq://wolverine.rmq.cloudamqp.com/vbknszsj"), h =>
                    {
                        h.Username("vbknszsj");
                        h.Password("QjAPX0m6ehoMIoUPdMmqzRmVGMNZIPcD");
                    });
                });

                return busControl;
            })
                .SingleInstance()
                .As<IBusControl>()
                .As<IBus>();
                
3- Inject IBusControl
        private readonly IBusControl _bus;
        public Constructor(IBusControl bus)
        {
          _bus = bus;
        }
4- Publish Message
_bus.Publish<ISampleMessage>(new MessageModel { Message = "Something" });

------------ WebAppFullFrameworkWithAutofac End--------------

---------------- DotnetCoreConsoleConsumerWithMasstransit Start---------------------
Using Masstransit in a dotnet core app to consume a message
1- add this packages

    <PackageReference Include="CoreConsoleHosting" Version="1.0.2" />
    <PackageReference Include="MassTransit.Extensions.DependencyInjection" Version="5.1.2" />
    <PackageReference Include="MassTransit.RabbitMQ" Version="5.1.2" />
2- add a startup and inherit from IConsoleStartup

3- in the program.cs add following code

    class Program
    {
        static void Main(string[] args)
        {
            BuildConsoleApp(args).Run();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="args"></param>
        /// <returns></returns>
        public static IConsoleApp BuildConsoleApp(string[] args) =>
             ConsoleApp.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
    
4- Register masstransit in startup
Find in the app
add your consumer and consume messages


---------------- DotnetCoreConsoleConsumerWithMasstransit End---------------------